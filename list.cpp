#include <iostream>
#include "list.h"

List::~List() {
	for(Node *p; !isEmpty(); ){
		p=head->next;
		delete head;
		head=p;
	}
}

void List::headPush(int value)
{
	//Add an element to the front of the list.
	Node *create = new Node(value, head, NULL);
	head = create;
	if(tail == 0)
	{
		tail = head;
	}
}

void List::tailPush(int value)
{
	//Add an element to the end of the list.
	Node *create = new Node(value, NULL, tail);
	if(tail != 0)
	{
		tail->next = create;
	}
	tail = create;
}

int List::headPop()
{
	//Remove an element of the front of the list, return a value stored in that node.
	if(head == 0)
	{
		return 0;
	}
	int value = head->info;
	if(head == tail)
	{
		delete tail;
		tail = 0;
		head = 0;
	}
	else
	{
		Node *del;
		del = head;
		head = del->next;
		head->prev = NULL;
		delete del;
	}
	return value;
}

int List::tailPop()
{
	//Remove an element of the end of the list, return a value stored in that node.
	if(tail == 0)
	{
		return 0;
	}
	int value = tail->info;
	if(tail == head)
	{
		delete tail;
		tail = 0;
		head = 0;	
	}
	else
	{
		Node *del = tail->prev;
		delete tail;
		tail = del;
		tail->next = NULL;
	}
	return value;
}

void List::deleteNode(int value)
{
	//Remove an element that contains the exact value.
	Node *del = head, *temporary = 0, *temporary2 = 0;
	if(head->info == value)
	{
		headPop();
	}
	else if(tail->info == value)
	{
		tailPop();
	}
	else
	{
		del = del->next;
		temporary = head;
		temporary2 = del->next;
		while(del != tail)
		{
			if(del->info == value)
			{
				temporary->next = temporary2;
				temporary2->prev = temporary;
				delete del;
				break;
			}
			temporary = del;
			del = temporary2;
			temporary2 = temporary2->next;
		}
	}
}

bool List::isInList(int value)
{
	//Find and return whether there's this value in the list.
	Node *index = head;
	while(index != NULL)
	{
		if(index->info == value)
		{
			return true;
		}
		index = index->next;
	}
	return false;
}

void List::showAll()
{
	//Display all elements in the list.
	Node *index = head;
	while(index != NULL)
	{
		std::cout << index->info << " ";
		index = index->next;
	}
}

template <typename T>
void List::swap_values(T& variable1, T& variable2)
{
	T temp;
	temp = variable1;
	variable1 = variable2;
	variable2 = temp;
}

int List::index_of_smallest(const int a[], int start_index, int number_used)
{
	int min = a[start_index];
	int index_of_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
		if (a[index] < min)
		{
			min = a[index];
			index_of_min = index;
		}
	return index_of_min;
}

void List::sort()
{
	//Sort all elements in the list.
	Node *pointer = head;
	bool isSwapped = false;
	while (true) 
	{
		isSwapped = false;
		while (pointer != tail) 
		{
			if (pointer->info > pointer->next->info) 
			{
				swap_values(pointer->next->info, pointer->info);
				isSwapped = true;
			} 
			pointer = pointer->next;
		}
		if (!isSwapped) 
		{
			return;
		}	
		pointer = head;
	}

}

void List::unique() 
{
	//Remove all duplicated elements in the list.
	Node *pointer1 = head;
	Node *pointer2 = head;
	int del;
	while (true) 
	{
		while (true) 
		{
			if (pointer1 != pointer2) 
			{
				if (pointer1->info == pointer2->info) 
				{
					del = pointer1->info;
					pointer1 = pointer1->next;
					List::deleteNode(del);
				}
			}
			if (pointer2->next == 0) 
			{
				break;
			}	
			pointer2 = pointer2->next;
		}
		pointer1 = pointer1->next;
		if (pointer1 == 0) return;
		pointer2 = head;
	}
}
